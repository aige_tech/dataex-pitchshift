# 数据扩充（变调）工具  
## 使用方法：  
`python3 pitchShift.py 样本路径 保存目录`  
例如：  
`python3 pitchShift.py test.txt test`  
批量处理：
`mkdir ../out`  
`find ./ -name *.txt -type f -exec python3 pitchShift.py {} ../out/{}\;`  